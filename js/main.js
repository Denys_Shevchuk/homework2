'use strict';
window.onload = function () {

    if (localStorage.getItem('bgcolor')) {
        document.body.className = 'style1';
    }
    document.getElementById('SuperButton').onclick = function () {

        if (document.body.className !== 'style1') {
            document.body.className = 'style1';
            localStorage.setItem('bgcolor', 'black');

        } else {
            document.body.className = 'style2';
            localStorage.clear();

        }
    }
};


